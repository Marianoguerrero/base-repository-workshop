import org.testng.annotations.Test;
import pageobjects.HomeML;

public class HomeMLTest extends BaseTest {

    @Test
    public void buscarAutoNuevo(){
        MainMLTest.iraMLDominincana();
        HomeML homeMLPage = new HomeML(driver);

        homeMLPage.buscarEnML("Auto nuevo");
    }
}
