import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class Practica extends BaseTest {

    @Test
    public void prueba() throws Exception {
        driver.get("http://www.mercadolibre.com");

        WebElement argentina = driver.findElement(By.id("AR"));

        argentina.click();

        WebElement busquedaInput = driver.findElement(By.xpath("/html/body/header/div/form/input"));

        busquedaInput.sendKeys("casa nueva");

        WebElement lupaButton = driver.findElement(By.xpath("/html/body/header/div/form/button"));

        generarLog(true , "Busco casa nueva");

        lupaButton.click();

        Assert.assertEquals(driver.getTitle(), "Casa Nueva en Casas en Mercado Libre Argentina");
        System.out.println(driver.getTitle());

    }

    @Test
    public void pruebaWait() throws InterruptedException {

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://www.toolsqa.com/automation-practice-switch-windows/");


        String tituloPrimero = driver.getTitle();

        WebDriverWait wait = new WebDriverWait(driver,5);
        wait.until(ExpectedConditions.titleIs("A new title is here"));

        String tituloSegundo = driver.getTitle();


        System.out.println(tituloPrimero);
        System.out.println(tituloSegundo);


       // String ventanaBase = driver.getWindowHandle();
//
       WebElement cookies = driver.findElement(By.id("cookie_action_close_header"));
       cookies.click();
//
       // WebElement botonDeVentanas = driver.findElement(By.id("button1"));
       // botonDeVentanas.click();
//
       // for (String ventana :driver.getWindowHandles()) {
       //     driver.switchTo().window(ventana);
       // }
       // driver.close();
//
       // driver.switchTo().window(ventanaBase);

        Thread.sleep(1000);
        WebElement clickToAlert = driver.findElement(By.id("alert"));
        clickToAlert.click();
        Thread.sleep(1000);

        Alert alert = driver.switchTo().alert();



        alert.accept();

    }

}
