import com.relevantcodes.extentreports.LogStatus;
import org.testng.annotations.Test;
import pageobjects.MainML;

public class MainMLTest extends BaseTest {

    @Test
    public static void iraMlArgentina(){
        MainML mainMLPage = new MainML(driver);
        mainMLPage.irAMLArgentina();

}

    @Test
    public static void iraMLDominincana(){
        MainML mainML = new MainML(driver);
        mainML.irAMLDominicana();
    }
}
