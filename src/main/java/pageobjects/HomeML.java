package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomeML extends  BasePage {
    public HomeML(WebDriver driver) {
        super(driver);
    }

    private By buscador = By.xpath("/html/body/header/div/form/input");
    private By searchButton = By.xpath("/html/body/header/div/form/button");


    public void buscarEnML(String elementoABuscar){
        driver.findElement(buscador).sendKeys(elementoABuscar);
        driver.findElement(searchButton).click();
    }

}
