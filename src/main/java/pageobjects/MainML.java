package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MainML extends BasePage {
    public MainML(WebDriver driver) {
        super(driver);
        driver.get("http://www.mercadolibre.com");
    }

    private By mlArgentina = By.id("AR");
    private By mlDominicana = By.id("DO");



    public void irAMLArgentina(){
        driver.findElement(mlArgentina).click();
    }

    public void irAMLDominicana(){
        driver.findElement(mlDominicana).click();
    }

}
